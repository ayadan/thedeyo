$( document ).ready( function() {
    var locations = [
        { "title" : "Heneshod", "neighbors" : "[hon]edi",
            "west" : 6, "image" : "map-east.png",
            "description" : "Bíide líithin shod, i radimil be wi. Ham shinehal.",
            "cando": "u [shumáath]eth",
            "computer" : "Yoshinehal: \"(Me)ham with: nede.\""
            }, // east

        { "title" : "Honeshod", "neighbors" : "[hene]di",
            "east" : 6, "image" : "map-west.png",
            "description" : "Bíide edutheshod nu. Meham shinedal.",
            "computer" : "Yoshinehal: \"▙▝░. Bel ne elotheledaleth? [em] [ra]:\"",
            }, // west

        { "title" : "Huneshod", "neighbors" : "[han]edi",
            "south" : 6, "image" : "map-north.png",
            "description" : "Bíide shidashod nu. Meham shinedal i théle.",
            "computer" : "Yoshinehal: \"Bíi thad il ne: Methal rul, Ralhoham, Roh┋▓▗- Bíi thad elash ne: Zhor, Holoshal Haw, ╧╬▒...\""
            }, // north

        { "title" : "Haneshod", "neighbors" : "[hun]edi",
            "north" : 6, "image" : "map-south.png",
            "description" : "Bíide mehin dahaneshod nuha. Rahume ben.",
            "computer" : "Yoshinehal: \"Shod 1: Anush Shin, Shod 2: Rozh Morish, Shod 3: Rebeha Zhal, Sho▚╤┅\""
            }, // south

        { "title" : "Rayileshod", "neighbors" : "[heb]edi",
            "down" : 6, "image" : "map-above.png",
            "description" : "Bíide shod thamehal wi, i merahíya dem. Menáwí meda nuha.",
            "computer" : "Yoshinehal: \"Medalam: thal. Meham: udemeda, medalayun, ilimeda.\""
            }, // above

        { "title" : "Yileshod", "neighbors" : "[raheb]edi",
            "up" : 6, "image" : "map-below.png",
            "description" : "Bíide meham worahíya wodim.",
            "computer" : "Yoshinehal: \"Lamith le dimeth... 4... 65... 102... 235... 255.\""
            }, // below

        { "title" : "Hathameshod", "neighbors" : "[hon]edi, [hene]di, [han]edi, [hun]edi, [heb]edi, [raheb]edi",
            "image" : "map-center.png",
            "down" : 5, "up" : 4, "north" : 2, "south" : 3, "east" : 0, "west" : 1,
            "description" : "Bíide ramihí hathameshod, izh rahume be.",
            "computer" : "Yoshinehal: \"Bíid eril ╳╓╵ le.\""
            }, // center
    ];

    var currentLocation = locations[0];
    var gameover = false;
    var havething = false;
    var prompt = false;
    var win = false;
    var lose = false;

    function DisplayLocation( first )
    {
        if ( first == true )
        {
            AppendOutput( "Yoshinehal: \"Wil sha, M╇▒▊. Ril nosháad ne ▓▁░▝ha.\"\n" );
        }
        else
        {
            AppendOutput( "\n" );
        }
        AppendOutput( currentLocation["title"] );
        $( "#game-map" ).attr( "src", "content/images/" + currentLocation["image"] );

        var lineString = "";
        for ( var i = 0; i < currentLocation["title"].length; i++ )
        {
            lineString += "-";
        }
        AppendOutput( lineString );        
        AppendOutput( currentLocation["description"] );
        AppendOutput( "\nBíide thad ne sháad: " );
        AppendOutput( currentLocation["neighbors"] );
        AppendOutput( "\nBíide thad ne shub: " );
        var cando = "duth [shinehal]eth"
        if ( typeof( currentLocation["cando"] ) !== "undefined" )
        {
            cando += ", " + currentLocation["cando"];
        }
        AppendOutput( cando );
    }

    function EnglishToLaadan( direction )
    {
        direction = direction.toLowerCase();

        if      ( direction == "north" )     { return "hun"; }
        else if ( direction == "south" )     { return "han"; }
        else if ( direction == "west" )      { return "hon"; }
        else if ( direction == "east" )      { return "hene"; }
        else if ( direction == "up" )        { return "raheb"; }
        else if ( direction == "down" )      { return "heb"; }
        else if ( direction == "computer" )  { return "shinehal"; }
        return "";
    }

    function AppendOutput( text )
    {
        var output = $( "#game-out" );
        output.html( $( "#game-out" ).html() + "\n" + text );

        if(output.length)
            output.scrollTop(output[0].scrollHeight - output.height());
    }

    function StringContains( fullString, subString )
    {
        return ( fullString.indexOf( subString ) >= 0 );
    }

    function MoveTo( index )
    {
        currentLocation = locations[ index ];
    }

    function Move( input )
    {
        var moveSuccess = false;
        if ( StringContains( input, EnglishToLaadan( "north" ) ) && typeof( currentLocation["north"] ) !== "undefined" )
        {
            MoveTo( currentLocation["north"] );
            moveSuccess = true;
        }
        else if ( StringContains( input, EnglishToLaadan( "south" ) ) && typeof( currentLocation["north"] ) !== "undefined" )
        {
            MoveTo( currentLocation["north"] );
            moveSuccess = true;
        }
        else if ( StringContains( input, EnglishToLaadan( "east" ) ) && typeof( currentLocation["east"] ) !== "undefined" )
        {
            MoveTo( currentLocation["east"] );
            moveSuccess = true;
        }
        else if ( StringContains( input, EnglishToLaadan( "west" ) ) && typeof( currentLocation["west"] ) !== "undefined" )
        {
            MoveTo( currentLocation["west"] );
            moveSuccess = true;
        }
        else if ( StringContains( input, EnglishToLaadan( "up" ) ) && typeof( currentLocation["up"] ) !== "undefined" )
        {
            MoveTo( currentLocation["up"] );
            moveSuccess = true;
        }
        else if ( StringContains( input, EnglishToLaadan( "down" ) ) && typeof( currentLocation["down"] ) !== "undefined" )
        {
            MoveTo( currentLocation["down"] );
            moveSuccess = true;
        }

        if ( moveSuccess )
        {
            AppendOutput( "\nBíide sháad ne núudi." );
            DisplayLocation();
        }
        else
        {
            AppendOutput( "\nBíide thad ra sháad ne núudi!" );
        }
    }

    function Computer()
    {
        if ( !havething )
        {
            AppendOutput( "\n" + currentLocation["computer"] );
        }
        else
        {
            var badchars = ["▂","░","▒","▓","▖","▚","▜","╫","┫"];
            var str = "";
            var length = Math.floor( Math.random() * 10 ) + 5;
            for ( var i = 0; i < length; i++ )
            {
                var rand = Math.floor( Math.random() * badchars.length );
                str += badchars[ rand ];
                console.log( rand );
            }
            AppendOutput( "\nYoshinehal: \"" + str + "\"" );
        }
    }

    function Special( input )
    {
        if ( currentLocation["title"] == "Heneshod" && prompt == false &&
                ( StringContains( input, "shumáath" ) || StringContains( input, "shumaath" ) ) )
        {
            // [em] [ra]
            AppendOutput( "\nUne ne yomeshumeyeneth? [em] [ra]" );
            prompt = true;
        }
        else if ( currentLocation["title"] == "Heneshod" && prompt == true )
        {
            if ( input == "em" )
            {
                AppendOutput( "\nBíide u ne shumáatheth i shumáad ne yode yodi netho." );
                if ( havething )
                {
                    AppendOutput( "\nBíide thi ne elotheledaleth. Shumáad ne nude." );
                    AppendOutput( "" );
                    AppendOutput( "----------------------------------------------" );
                    AppendOutput( "- NOHELASH NE SHIDATH: Thi ne elotheledaleth -" );
                    AppendOutput( "----------------------------------------------" );
                    win = true;
                }
                else
                {
                    AppendOutput( "\nBíide thad ra rede ne elotheledaleth. Shumáad ne nude." );
                    AppendOutput( "\nBíide thi ne elotheledaleth. Shumáad ne nude." );
                    AppendOutput( "" );
                    AppendOutput( "----------------------------------" );
                    AppendOutput( "- NOHELASH NE SHIDATH: Nóduredeb -" );
                    AppendOutput( "----------------------------------" );
                    lose = true;
                }
            }
            else
            {
                AppendOutput( "\nBíide u ne shumáatheth i shumáad ne delinedi. Thad ra wíyul ne." );
                AppendOutput( "" );
                AppendOutput( "------------------------------" );
                AppendOutput( "- NOHELASH NE SHIDATH: Rawíi -" );
                AppendOutput( "------------------------------" );
                gameover = true;
                $( "#game-map" ).attr( "src", "content/images/map-dead.png" );
            }
        }
        else if ( currentLocation["title"] == "Honeshod" && StringContains( input, "ra" ) )
        {
            AppendOutput( "\nBíide bel ra le elotheledaleth." );
        }
        else if ( currentLocation["title"] == "Honeshod" && StringContains( input, "em" ) )
        {
            AppendOutput( "\nBíide bel le elotheledaleth." );
            havething = true;
        }
        else
        {
            AppendOutput( "\nBáa bebáa\"" + input + "\"?!" );
        }
    }

    function HandleInput( input )
    {
        if ( StringContains( input, "hon" ) || StringContains( input, "hen" ) ||
             StringContains( input, "han" ) || StringContains( input, "hune" ) ||
             StringContains( input, "heb" ) || StringContains( input, "raheb" )  )
        {
            Move( input );
        }
        else if ( StringContains( input, "shinehal" ) )
        {
            Computer();
        }
        else
        {
            Special( input );
        }
    }

    $( "#player-input" ).keydown( function( ev ) {
        if ( ev.keyCode == 13 )
        {
            // Enter
            if ( gameover )
            {
                AppendOutput( "\n\nBíide rawíi ne - thad ra shub ne hith!" );
            }
            else if ( win )
            {
            }
            else if ( lose )
            {
            }
            else
            {
                var input = $( "#player-input" ).val();
                HandleInput( input );
            }
            $( "#player-input" ).val( "" );
        }
    } );

    DisplayLocation( true );
} );
